package nl.utwente.di.bookQuote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** * Tests the Quoter*/
public class TestQuoter {

    @Test
    public void testBook1() throws Exception{
        Quoter quoter = new Quoter ();
        double price= quoter.getBookPrice("3");
        Assertions.assertEquals(20.0, price, 0.0,"Price of book 1");


    }
}
