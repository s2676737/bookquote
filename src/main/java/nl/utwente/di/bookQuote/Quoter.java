package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn) {
        HashMap<String, Double> hm= new HashMap<String, Double>();
        hm.put("1", 999999.0);
        hm.put("2", 45.0);
        hm.put("3", 20.0);
        hm.put("4", 50.0);
        System.out.println("it reaches here");

        if (hm.containsKey(isbn)){

            return hm.get(isbn);
        }

        return 0.0;

    }
}
